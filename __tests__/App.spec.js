import * as React from 'react'
import renderer from 'react-test-renderer';
import { shallow, mount, configure } from 'enzyme';
import Adapter from "enzyme-adapter-react-16/build";

import App from "../src/App";
import Head from "../src/Head";

configure({ adapter: new Adapter() });


describe('Components', () => {
    describe('App', () => {
        let wrap;
        beforeEach(() => wrap = mount(<App/>));

        it('should render correctly', function () {
            const tree = renderer.create(<App/>);
            expect(tree.toJSON()).toMatchSnapshot();
        });

        it('should contain Head', function () {
            expect(wrap.contains(<Head/>)).toBe(true);
        });
    });
});
