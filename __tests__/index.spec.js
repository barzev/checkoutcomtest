import * as React from 'react'
import renderer from 'react-test-renderer';
import { shallow, configure } from 'enzyme'
import Adapter from "enzyme-adapter-react-16/build";

import Index from '../pages/index'
import App from "../src/App";

configure({ adapter: new Adapter() });

describe('Pages', () => {
    describe('Index', () => {
        it('should render correctly', function () {
            const tree = renderer.create(<Index/>);
            expect(tree.toJSON()).toMatchSnapshot();
        });

        it('should contain our App component and only that', function () {
            const wrap = shallow(React.createElement(Index, {}));
            expect(wrap.contains(<App/>)).toBe(true);
        });
    });
});
