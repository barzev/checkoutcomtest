import * as React from 'react'
import renderer from 'react-test-renderer';
import Adapter from "enzyme-adapter-react-16/build";
import { mount } from 'enzyme'
import { configure } from "enzyme/build";

import Graph from "../src/Graph";

configure({ adapter: new Adapter() });

console.error = jest.fn();

const fakeData = [{ name: 'January', uv: 4000, pv: 2400, amt: 2400, }];

describe('Components', () => {
    describe('Graph', () => {

        beforeEach(() => {
            console.error.mockClear();
        });

        it('should render correctly', function () {
            const tree = renderer.create(<Graph/>);

            expect(tree.toJSON()).toMatchSnapshot();
        });

        it('should handle no data/undefined props gracefully', function () {
            mount(<Graph data={[]}/>);
            expect(console.error).toHaveBeenCalledTimes(0);
        });

        it('should handle actual data props successfully', function () {
            const wrap = mount(<Graph data={fakeData}/>);
            expect(wrap.find('tspan').contains('January')).toBe(true);
        });
    });
});
