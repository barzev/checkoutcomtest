import * as React from 'react';
import renderer from 'react-test-renderer';
import Adapter from 'enzyme-adapter-react-16';
import { mount, configure } from 'enzyme';

import Layout from "../src/Layout";

configure({ adapter: new Adapter() });

describe('Components', () => {
    describe('Layout', () => {
        let wrap;
        let testProps;

        beforeEach(() => {
            testProps = [];
            wrap = mount((
                <Layout>
                    {testProps}
                </Layout>
            ));
        });

        it('should render correctly', function () {
            const tree = renderer.create((
               <Layout>
                   {testProps}
               </Layout>
            ));

            expect(tree.toJSON()).toMatchSnapshot();
        });

        it('should render only div wrapper without any props', () => {
            wrap = mount(<Layout />);

            expect(wrap.find('.mainLayout').children()).toHaveLength(0);
        });

        it('should correctly render the supplied props', () => {
            const nodeEls = [<div key="1">hiDiv</div>, <p key="2">hiP</p>];

            const outerWrap = mount((
                <Layout>
                    {nodeEls}
                </Layout>
            ));

            const innerDiv = outerWrap.find('.mainLayout');
            expect(innerDiv.children()).toHaveLength(2);
            expect(innerDiv.children().find('div')).toHaveLength(1);
            expect(innerDiv.children().find('p')).toHaveLength(1);
        });
    });
});
