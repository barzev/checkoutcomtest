# CheckoutComTest

Next.js SSR (Server-Side-Rendering React) + CSS + Sass + ReactStrap + ReCharts.js Sample

# Pre-requisites
Node.js


# How to run

1. Navigate to root of project
2. npm install
3. npm run dev
4. Hit localhost:3000 in your browser

# Running tests
1. Navigate to root of project
2. npm install
3. npm run test