import React from "react";
import { CartesianGrid, Line, LineChart, XAxis, YAxis } from "recharts";

export default ({ data: fakeData }) => {
    return (
        !!fakeData
            ? <React.Fragment>
                <h3>Comments monthly rates</h3>
                <LineChart width={900} height={250} data={fakeData}>
                    <XAxis dataKey="name"/>
                    <YAxis/>
                    <CartesianGrid stroke="#eee" strokeDasharray="5 5"/>
                    <Line type="monotone" dataKey="uv" stroke="#8884d8"/>
                    <Line type="monotone" dataKey="pv" stroke="#82ca9d"/>
                </LineChart>
            </React.Fragment>
            : null
)
};
