import React from "react";
import { Col, Form, FormGroup, Input, InputGroup, InputGroupAddon, Label } from "reactstrap";
import Rating from "react-rating";


const UserForm = () => (
    <Form>
        <h3>User details</h3>
        <FormGroup row>
            <Label for="name" sm={2}>Name</Label>
            <Col sm={10}>
                <Input type="name" name="name" id="name" placeholder="Name"/>
            </Col>
        </FormGroup>
        <FormGroup row>
            <Label for="email" sm={2}>Password</Label>
            <Col sm={10}>
                <InputGroup>
                    <InputGroupAddon addonType="prepend">@</InputGroupAddon>
                    <Input
                        name="email"
                        id="email"
                        placeholder="Email"
                        aria-label="Email"
                        aria-describedby="basic-addon1"
                    />
                </InputGroup>
            </Col>
        </FormGroup>
        <FormGroup row>
            <Label for="rating" sm={2}>Rating</Label>
            <Col sm={10}>
                <Rating name="rating"/>
            </Col>
        </FormGroup>
        <FormGroup row>
            <Label for="comment" sm={2}>Comment</Label>
            <Col sm={10}>
                <Input type="textarea" name="comment" id="comment" />
            </Col>
        </FormGroup>
    </Form>
);

export default UserForm;
