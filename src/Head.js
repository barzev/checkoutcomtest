import Head from 'next/head';
import React from "react";

export default () => (
    <div>
        <Head title="React Checkout.com test">
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/fontawesome.css"
                  integrity="sha384-4aon80D8rXCGx9ayDt85LbyUHeMWd3UiBaWliBlJ53yzm9hqN21A+o1pqoyK04h+"
                  crossOrigin="anonymous"/>
            <link href="https://fonts.googleapis.com/css?family=Roboto:500,500i" rel="stylesheet" />
            <title>Checkout.com React Test</title>
            <meta name="viewport" content="width=device-width, initial-scale=1"/>
            <meta charSet="utf-8"/>
        </Head>
    </div>
);
