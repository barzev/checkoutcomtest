import React from "react";
import { ListGroup, ListGroupItem } from "reactstrap";

export default () => (
    <div style={allCommentsStyles}>
        <h3>Latest comments</h3>
        <ListGroup>
            <ListGroupItem>Sample comment 1</ListGroupItem>
            <ListGroupItem>Sample comment 2</ListGroupItem>
            <ListGroupItem>Sample comment 3</ListGroupItem>
            <ListGroupItem>Sample comment 4</ListGroupItem>
            <ListGroupItem>Sample comment 5</ListGroupItem>
        </ListGroup>
    </div>
);

const allCommentsStyles = {
    marginTop: '50px'
};
